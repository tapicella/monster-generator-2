import ability
import attribute
import mongen_utilities as mu
__metaclass__ = type

class species:
    def __init__(self, n=""):
        self.name=n
        self.desc=""
        self.abilities=set()
        self.attributes=set()
        
    def fprint(self, indent, ignore_derived=False):
        print indent+"Name: "+self.name
        print indent+"Description: "+self.desc
        if not ignore_derived:
            print indent+"Innate attributes: "
            for attribute in self.attributes:
                if attribute!=None: attribute.fprint(indent+indent, True)
            print indent+"Abilities granted: "
            for ability in self.abilities:
                if ability!=None: ability.fprint(indent+indent)
        print

def detect_quit(spec):
    if "quit" in spec:
        return True
    else:
        for s in spec:           
            if s!=None:
                if attribute.detect_quit(s.attributes):
                    return True
                elif ability.detect_quit(s.abilities):
                    return True
    return False
 
def make_multiple_species(mon_db, source_class, indent=""):
    species_str = raw_input(indent+"Enter the species of this "+source_class+", separated by semicolons: ")
    if species_str == "": return []
    elif mu.check_quit(species_str): 
        quit_set = set()
        quit_set.add("quit")
        return quit_set
    species_str = species_str.replace(",", ";")
    species_str_array = species_str.split(";")
    species_set = set()
    for e in species_str_array:
        e = e.strip()
        new_species = make_species(mon_db, e, derived=True, indent=indent)
        if type(new_species)==type("quit"):
            if new_species == "quit":
                quit_set = set()
                quit_set.add("quit")
                return quit_set
            else:
                raise TypeError
        species_set.add(new_species)
    if None in species_set: species_set.remove(None)
    return species_set 
    
def make_species(mon_db, name="", derived=False, indent=""):
    new_species = species(name)
    while mon_db.check_if_exists(new_species, warn=False)==True:
        
        if derived==False:
            new_species.name = raw_input(indent+"Enter the full name of your species, or type quit to cancel: ").lower()
            if mu.check_quit(new_species.name): return
            if not mon_db.check_if_exists(new_species):
                break
        else:
            if name!="":
                print indent+"Found species "+name+". Adding."
                new_species = mon_db.species[name]
                return new_species
            else:
                return
        

    new_species.desc = raw_input(indent+"Enter a description of "+new_species.name+": ")
    new_species.attributes = set(attribute.make_attributes(mon_db, "species: "+new_species.name, indent+"    "))
    if "quit" in new_species.attributes:
        if derived==True: return "quit"
        else: return
    new_species.abilities = set(ability.make_abilities(mon_db, "species: "+new_species.name, indent+"    "))
    if "quit" in new_species.abilities:
        if derived==True: return "quit"
        else: return
        
    print indent+"Adding derived abilities"
    for att in new_species.attributes:
        for ab in att.abilities:
            new_species.abilities.add(ab)
    mon_db.check_if_exists(new_species, addme=True)  
    return new_species