import ability
import mongen_utilities as mu
__metaclass__ = type

class attribute:
    def __init__(self, n=""):
        self.name=n
        self.desc=""
        self.abilities=set()
    
    
    def fprint(self, indent,ignore_derived=False):
        print indent+"Name: "+self.name
        print indent+"Description: "+self.desc
        if not ignore_derived:
            print indent+"Abilities granted: "
            for ability in self.abilities:
                if ability!=None: ability.fprint(indent+indent)
            print
        print

def detect_quit(att):
    if "quit" in att:
        return True
    else:
        for a in att:
            if a!=None:
                if ability.detect_quit(a.abilities):
                    return True
    return False
 
def make_attributes(mon_db, source_class, indent=""):
    attribute_str = raw_input(indent+"Enter the attributes of this "+source_class+", separated by semicolons: ")
    if attribute_str == "": return set()
    elif mu.check_quit(attribute_str): 
        quit_set = set()
        quit_set.add("quit")
        return quit_set
    attribute_str = attribute_str.replace(",", ";")
    attribute_str_array = attribute_str.split(";")
    attribute_set = set()
    for e in attribute_str_array:
        e = e.strip()
        new_attribute = make_attribute(mon_db, e, derived=True, indent=indent)
        if type(new_attribute)==type("quit"):
            if new_attribute == "quit":
                quit_set = set()
                quit_set.add("quit")
                return quit_set
            else:
                raise TypeError
        attribute_set.add(new_attribute)
    if None in attribute_set: attribute_set.remove(None)
    return attribute_set 
    
def make_attribute(mon_db, name="", derived=False, indent=""):
    new_attribute = attribute(name)
    while mon_db.check_if_exists(new_attribute, warn=False)==True:
        
        if derived==False:
            new_attribute.name = raw_input(indent+"Enter the full name of your attribute, or type quit to cancel: ").lower()
            if mu.check_quit(new_attribute.name): return
            if not mon_db.check_if_exists(new_attribute):
                break
        else:
            if name!="":
                print indent+"Found attribute "+name+". Adding."
                new_attribute = mon_db.attributes[name]
                return new_attribute
            else:
                return
        

    new_attribute.desc = raw_input(indent+"Enter a description of "+new_attribute.name+": ")
    new_attribute.abilities = ability.make_abilities(mon_db, "attribute: "+new_attribute.name,indent+"    ")
    if "quit" in new_attribute.abilities:
        if derived==True: return "quit"
        else: return
    mon_db.check_if_exists(new_attribute, addme=True)  
    return new_attribute