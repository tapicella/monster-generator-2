import ability
import attribute
import biome
import element
import species
import random
import mongen_utilities as mu
__metaclass__ = type


class monster:
    def __init__(self, n=""):
        self.name=n
        self.desc=""
    
        self.abilities=set()
        self.attributes=set()
        self.biomes=set()
        self.elements=set()
        self.intelligence=None
        self.origin=None
        self.species=set()
        
    def fprint(self, indent="", ignore_derived=False):
        print "Name: "+self.name
        print indent+"Description: "+self.desc
        print indent+"Origin: "+self.origin.name
        print indent+"Intelligence class: "+self.intelligence.name
        print indent+"Biomes inhabited: "
        for biome in self.biomes:
            if biome!=None: biome.fprint(indent+indent)
        print indent+"Elemental types: "
        for element in self.elements:
            if element!=None: element.fprint(indent+indent)
        print indent+"Monster species: "
        for spec in self.species:
            if spec!=None: spec.fprint(indent+indent, True)
        print indent+"Innate attributes: "
        for att in self.attributes:
            if att!=None: att.fprint(indent+indent, True)
        print indent+"Monster abilities: "
        for ab in self.abilities:
            if ab!=None: ab.fprint(indent+indent)    

def show_current_traits(new_monster):
    print
    if new_monster.origin!=None:
        print "    Origin: "+new_monster.origin.name
    if new_monster.intelligence!=None:
        print "    Intelligence class: "+new_monster.intelligence.name
        
    if len(new_monster.elements)>0:
        e_str = ""
        for e in new_monster.elements:
            if e!=None: e_str += e.name+"  "
        print "    Elements: "+e_str
        
    if len(new_monster.biomes)>0:
        b_str = ""
        for b in new_monster.biomes:
            if b!=None: b_str += b.name+"  "
        print "    Biomes: "+b_str
        
    if len(new_monster.species)>0:
        s_str = ""
        for s in new_monster.species:
            if s!=None: s_str += s.name+"  "
        print "    Species: "+s_str
        
    if len(new_monster.attributes)>0:
        a_str = ""
        for a in new_monster.attributes:
            if a!=None: a_str += a.name+"  "
        print "    Attributes: "+a_str
    
    if len(new_monster.abilities)>0:
        a_str = ""
        for a in new_monster.abilities:
            if a!=None: a_str += a.name+"  "
        print "    Abilities: "+a_str
    print
    
def make_monster_prompt(mon_db, name="", indent="    "):
    random.seed()
    
    orgs = ["natural", "magical", "demonic", "nightmare", "artificial"]
    ints = ["mindless", "feral", "trainable", "communicative", "semi-sentient", "sentient", "superhuman"]
    
    r_org = random.randint(-3,4)
    if r_org<=0:
        r_int = random.randint(-1,6)
    else:
        r_int = random.randint(-3,6)
    
    be_un_weight = [0,0,0,0,0,0,0,0]
    biome_weight = [1, 1, 1, 1, 1, 1, 1, 1, 2, 2]    
    elem_weight = [0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3]
    
    as_un_weight = [0,0,0,0,0,0,0,0]
    spec_weight = [1,1,1,1,1,1,2,2]
    att_weight = [0, 1, 1, 2, 2, 3, 3 ,4, 4]
    
    if r_org>=0 and r_int >= 0:
        elem_weight.extend(be_un_weight)
    elem_count = random.choice(elem_weight)
    
    if elem_count>=2:
        biome_weight.extend(be_un_weight)
    biome_count = random.choice(biome_weight)
    
    if elem_count+biome_count >= 3:
        spec_weight.extend(as_un_weight)
    spec_count = random.choice(spec_weight)
    
    if spec_count==2:
        att_weight.extend(as_un_weight)
    att_count = random.choice(att_weight)  
      
    new_monster = monster()
    
    if r_org >=0: new_monster.origin = mon_db.origin[orgs[r_org]]
    if r_int >=0: new_monster.intelligence = mon_db.intelligence[ints[r_int]]
    
    choice_elements = mon_db.elements.keys()
    elem_count = min(elem_count, len(choice_elements))
    for i in range(elem_count):
        new_monster.elements.add(mon_db.elements[random.choice(choice_elements)])
    
    choice_biomes = mon_db.biomes.keys()
    biome_count = min(biome_count, len(choice_biomes))
    for j in range(biome_count):
        new_monster.biomes.add(mon_db.biomes[random.choice(choice_biomes)])
    
    choice_species = mon_db.species.keys()
    spec_count = min(spec_count, len(choice_species))
    for k in range(spec_count):
        new_monster.species.add(mon_db.species[random.choice(choice_species)])
    
    choice_atts = mon_db.attributes.keys()
    att_count = min(att_count, len(choice_atts))
    if spec_count>0:
        base_att_count = 0
        for s in new_monster.species:
            base_att_count+= len(s.attributes)
        att_count = base_att_count-att_count+1
        if att_count < 0: att_count=1
    for l in range(att_count):
        new_monster.attributes.add(mon_db.attributes[random.choice(choice_atts)])
    
    #print "Adding derived attributes"
    for sp in new_monster.species:
        for att in sp.attributes:
            new_monster.attributes.add(att)
        for ab in sp.abilities:
            new_monster.abilities.add(ab)
    #print "Adding derived abilities"
    for att in new_monster.attributes:
        for ab in att.abilities:
            new_monster.abilities.add(ab)
    
    print "Create a monster with the following traits: "
    show_current_traits(new_monster)
        
    while mon_db.check_if_exists(new_monster, warn=False)==True:
        
        new_monster.name = raw_input("Enter the full name of your monster, or type quit to cancel: ").lower()
        if mu.check_quit(new_monster.name): return
        if not mon_db.check_if_exists(new_monster):
            break
        else:
            if name!="":
                print "Found monster "+name+". Adding."
                new_monster = mon_db.monsters[name]
                return new_monster
            else:
                return
        

    new_monster.desc = raw_input("Enter a description of "+new_monster.name+": ")
    while new_monster.origin==None:
        origin_string = raw_input("Enter the monster's origin (Natural, Magical, Demonic, Nightmare, Artificial)").lower()
        if mu.check_quit(origin_string): return
        try: 
            new_monster.origin = mon_db.origin[origin_string]
            break
        except KeyError: print "Invalid origin "+origin_string 
    while new_monster.intelligence==None:
        int_string = raw_input("Enter the monster's intelligence level (Mindless, Feral, Trainable, Communicative, Semi-sentient, Sentient, Superhuman)").lower()
        if mu.check_quit(int_string): return
        try: 
            new_monster.intelligence = mon_db.intelligence[int_string]
            break
        except KeyError: print "Invalid intelligence level "+int_string
    
    
    bi_length = len(new_monster.biomes)
    sp_length = len(new_monster.species)
    el_length = len(new_monster.elements)
    at_length = len(new_monster.attributes)
    
    
    if bi_length == 0:
        new_monster.biomes = set(biome.make_biomes(mon_db, "monster", indent))
        if biome.detect_quit(new_monster.biomes): 
            return
        if mu.check_yn(raw_input("Do you want to display the current parameters?")): show_current_traits(new_monster)

    if sp_length == 0:    
        new_monster.species = set(species.make_multiple_species(mon_db, "monster", indent))
        if species.detect_quit(new_monster.species):
            return
        if mu.check_yn(raw_input("Do you want to display the current parameters?")): show_current_traits(new_monster)

    
    if el_length == 0:
        new_monster.elements = set(element.make_elements(mon_db, "monster", indent))
        if element.detect_quit(new_monster.elements): 
            return
        if mu.check_yn(raw_input("Do you want to display the current parameters?")): show_current_traits(new_monster)
        
    new_monster.abilities = set(ability.make_abilities(mon_db, "monster", indent))
    if ability.detect_quit(new_monster.abilities):
        return
    if mu.check_yn(raw_input("Do you want to display the current parameters?")): show_current_traits(new_monster)
    
    if at_length == 0:
        new_monster.attributes = set(attribute.make_attributes(mon_db, "monster", indent))
        if attribute.detect_quit(new_monster.attributes):
            return
        
    
    print "Adding derived attributes"
    for sp in new_monster.species:
        for att in sp.attributes:
            new_monster.attributes.add(att)
        for ab in sp.abilities:
            new_monster.abilities.add(ab)
    print "Adding derived abilities"
    for att in new_monster.attributes:
        for ab in att.abilities:
            new_monster.abilities.add(ab)
    mon_db.check_if_exists(new_monster, addme=True)  
    return new_monster
    
    
    
def make_monster(mon_db, name="", indent="    "):
    new_monster = monster(name)
    while mon_db.check_if_exists(new_monster, warn=False)==True:
        
        new_monster.name = raw_input("Enter the full name of your monster, or type quit to cancel: ").lower()
        if mu.check_quit(new_monster.name): return
        if not mon_db.check_if_exists(new_monster):
            break
        else:
            if name!="":
                print "Found monster "+name+". Adding."
                new_monster = mon_db.monsters[name]
                return new_monster
            else:
                return
        

    new_monster.desc = raw_input("Enter a description of "+new_monster.name+": ")
    while True:
        origin_string = raw_input("Enter the monster's origin (Natural, Magical, Demonic, Nightmare, Artificial)").lower()
        if mu.check_quit(origin_string): return
        try: 
            new_monster.origin = mon_db.origin[origin_string]
            break
        except KeyError: print "Invalid origin "+origin_string 
    while True:
        int_string = raw_input("Enter the monster's intelligence level (Mindless, Feral, Trainable, Communicative, Semi-sentient, Sentient, Superhuman)").lower()
        if mu.check_quit(int_string): return
        try: 
            new_monster.intelligence = mon_db.intelligence[int_string]
            break
        except KeyError: print "Invalid intelligence level "+int_string
    new_monster.biomes = set(biome.make_biomes(mon_db, "monster", indent))
    if biome.detect_quit(new_monster.biomes): 
        return
    new_monster.species = set(species.make_multiple_species(mon_db, "monster", indent))
    if species.detect_quit(new_monster.species):
        return
    new_monster.elements = set(element.make_elements(mon_db, "monster", indent))
    if element.detect_quit(new_monster.elements): 
        return
    new_monster.abilities = set(ability.make_abilities(mon_db, "monster", indent))
    if ability.detect_quit(new_monster.abilities):
        return
    new_monster.attributes = set(attribute.make_attributes(mon_db, "monster", indent))
    if attribute.detect_quit(new_monster.attributes):
        return
    
    print "Adding derived attributes"
    for sp in new_monster.species:
        for att in sp.attributes:
            new_monster.attributes.add(att)
        for ab in sp.abilities:
            new_monster.abilities.add(ab)
    print "Adding derived abilities"
    for att in new_monster.attributes:
        for ab in att.abilities:
            new_monster.abilities.add(ab)
            
    mon_db.check_if_exists(new_monster, addme=True)
    try:
        new_monster.abilities.remove(None)
    except KeyError:
        pass
    
    try:
        new_monster.attributes.remove(None)
    except KeyError:
        pass
    
    try:
        new_monster.biomes.remove(None)
    except KeyError:
        pass
    
    try:
        new_monster.elements.remove(None)
    except KeyError:
        pass
    
    try:
        new_monster.species.remove(None)
    except KeyError:
        pass
      
    return new_monster
    