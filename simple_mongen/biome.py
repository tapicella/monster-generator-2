import mongen_utilities as mu
__metaclass__ = type


class biome:
        
    def __init__(self, n=""):
        self.name = n
        self.desc = ""
    def fprint(self, indent):
        print indent+"Name: "+self.name
        print indent+"Description: "+self.desc
        print
    
def detect_quit(biomes):
    if "quit" in biomes:
        return True
    else:
        return False

def make_biomes(mon_db, source_class, indent=""):
    biome_str = raw_input(indent+"Enter the biomes of this "+source_class+", separated by semicolons: ")
    if biome_str == "": return []
    elif mu.check_quit(biome_str): 
        quit_set = set()
        quit_set.add("quit")
        return quit_set
    biome_str = biome_str.replace(",", ";")
    biome_str_array = biome_str.split(";")
    biome_set = set()
    for e in biome_str_array:
        e = e.strip()
        new_biome = make_biome(mon_db, e, derived=True, indent=indent)
        if type(new_biome)==type("quit"):    
            if new_biome == "quit":
                quit_set = set()
                quit_set.add("quit")
                return quit_set
            else:
                raise TypeError
        biome_set.add(new_biome)
    
    if None in biome_set: biome_set.remove(None)
    return biome_set

def make_biome(mon_db, name="", derived=False, indent=""):
    new_biome = biome(name)
    while mon_db.check_if_exists(new_biome, warn=False)==True:
        if derived==False:
            new_biome.name = raw_input(indent+"Enter the full name of your biome, or type quit to cancel: ").lower()
            if mu.check_quit(new_biome.name): return
            if not mon_db.check_if_exists(new_biome):
                break
        else:
            if name!="":
                print indent+"Found biome "+name+". Adding."
                new_biome = mon_db.biomes[name]
                return new_biome
            else:
                return
    new_biome.desc = raw_input(indent+"Enter a description of "+new_biome.name+": ")
    mon_db.check_if_exists(new_biome, addme=True) 
    return new_biome