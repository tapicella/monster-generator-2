import element
import mongen_utilities as mu

__metaclass__ = type

class ability:
    def __init__(self, n=""):
        self.name=n
        self.shortname=""
        self.desc=""
        self.ability_type=""
        self.atk_range=""
        self.damage=""
        self.acc=""
        self.elements=set()
    
        
    def fprint(self, indent):
        print indent+"Unique name: "+self.name+"    Common name: "+self.shortname
        print indent+"Description: "+self.desc
        print indent+"Type: "+self.ability_type+"    Range: "+self.atk_range
        print indent+"Damage: "+self.damage+"     Accuracy: "+self.acc+"%"
        print indent+"Ability elements: "
        for element in self.elements:
            if element!=None: element.fprint(indent+indent)
        print

def detect_quit(abilities):
    if "quit" in abilities:
        return True
    else:
        for a in abilities:
            if a != None:
                if element.detect_quit(a.elements):
                    return True
    return False
        
def check_type(ability_type, indent=""):
    ability_types=["offensive", "defensive", "passive", "combat", "overworld"]      
    if ability_type in ability_types:
        return True
    elif ability_type=="":
        return False
    else:
        print indent+"Invalid value for type"
        return False   
      
def check_range(atk_range, indent=""):
    ranges=["short", "short-medium", "medium", "medium-long", "long", "all"]
    if atk_range in ranges:
        return True
    elif atk_range=="":
        return False
    else:
        print indent+"Invalid value for range"
        return False    

def check_number(num, num_type, indent=""):
    if num=="":
        return False
    else:
        try:
            int_num=int(num)
            if int_num >100 or int_num < 0:
                raise ValueError
            return True
        except ValueError:
            print indent+num_type+" must be a number between 0 and 100"
            return False

def make_abilities(mon_db, source_class, indent=""):
    ability_str = raw_input(indent+"Enter the abilities of this "+source_class+", separated by semicolons: ")
    if ability_str == "": return set()
    elif mu.check_quit(ability_str): 
        quit_set = set()
        quit_set.add("quit")
        return quit_set
    ability_str = ability_str.replace(",", ";")
    ability_str_array = ability_str.split(";")
    ability_set = set()
    for e in ability_str_array:
        e = e.strip()
        new_ability = make_ability(mon_db, e, derived=True, indent=indent)
        if type(new_ability) == type("quit"):
            if new_ability=="quit":
                quit_set = set()
                quit_set.add("quit")
                return quit_set
            else:
                raise TypeError
        ability_set.add(new_ability)
    
    if None in ability_set: ability_set.remove(None)
    return ability_set 
    
def make_ability(mon_db, name="", derived=False, indent=""):
    new_ability = ability(name)
    while mon_db.check_if_exists(new_ability, warn=False)==True:
        
        if derived==False:
            new_ability.name = raw_input(indent+"Enter the full name of your ability, or type quit to cancel: ").lower()
            if mu.check_quit(new_ability.name): return
            if not mon_db.check_if_exists(new_ability):
                break
        else:
            if name!="":
                print indent+"Found ability "+name+". Adding."
                new_ability = mon_db.abilities[name]
                return new_ability
            else:
                return
        

    new_ability.shortname = raw_input(indent+"Enter the short name of "+new_ability.name+": ").lower()
    if new_ability.shortname=="": new_ability.shortname = new_ability.name
    new_ability.desc = raw_input(indent+"Enter a description of "+new_ability.name+": ")
    while check_type(new_ability.ability_type)==False:
        new_ability.ability_type = raw_input(indent+"Enter the type of "+new_ability.name+" (offensive, defensive, passive, combat, overworld):")
    while check_range(new_ability.atk_range)==False:
        new_ability.atk_range = raw_input(indent+"Enter the range of "+new_ability.name+" (short, short-medium, medium, medium-long, long, all): ")
    while check_number(new_ability.damage, "Damage")==False:
        new_ability.damage = raw_input(indent+"Enter the base damage. Must be between 0 and 100: ")
    while check_number(new_ability.acc, "Accuracy")==False:
        new_ability.acc = raw_input(indent+"Enter the base accuracy. Must be between 0 and 100: ")
    
    new_ability.elements = element.make_elements(mon_db, "ability: "+new_ability.name, indent+"    ")
    if "quit" in new_ability.elements: 
        if derived==True: return "quit"
        else: return
    mon_db.check_if_exists(new_ability, addme=True)    
            
    return new_ability
    