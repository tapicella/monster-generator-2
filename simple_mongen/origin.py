import mongen_utilities as mu
__metaclass__ = type


class origin:
        
    def __init__(self, n="", d=""):
        self.name = n
        self.desc = d
    def fprint(self, indent):
        print indent+"Name: "+self.name
        print indent+"Description: "+self.desc
        print
        
#===============================================================================
# def make_origins(mon_db, source_class):
#     origin_str = raw_input("Enter the origins of this "+source_class+", separated by semicolons: ")
#     if origin_str == "": return []
#     origin_str = origin_str.replace(",", ";")
#     origin_str_array = origin_str.split(";")
#     origin_array = []
#     for e in origin_str_array:
#         e = e.strip()
#         new_origin = make_origin(mon_db, e, derived=True)
#         origin_array.append(new_origin)
#     return origin_array
#===============================================================================

def make_origin(mon_db, name="", derived=False):
    new_origin = origin(name)
    while mon_db.check_if_exists(new_origin, warn=False)==True:
        if derived==False:
            new_origin.name = raw_input("Enter the full name of your origin, or type quit to cancel: ").lower()
            if mu.check_quit(new_origin.name): return
            if not mon_db.check_if_exists(new_origin):
                break
        else:
            if name!="":
                print "Found origin "+name+". Adding."
                new_origin = mon_db.origins[name]
                return new_origin
            else:
                return
    new_origin.desc = raw_input("Enter a description of "+new_origin.name+": ")
    mon_db.check_if_exists(new_origin, addme=True) 
    return new_origin