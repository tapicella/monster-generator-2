'''
Created on Mar 31, 2014

@author: intern
'''

import mon_database as mdb
import mongen_utilities as mu
import ability
import attribute
import biome
import element
import intelligence
import origin
import species
import monster
import os
import cPickle as pickle

__metaclass__ = type

def wait():
    try: 
        raw_input("Press enter to continue")
    except SyntaxError:
        pass
    clear()

def clear(): print "\n" * 100
    
    
def main_menu(mon_db):
    while(True):
        print "Welcome! Please select an option:"
        print "0:  Make a new monster"
        print "1:  Make a new monster with prompt"
        print "2:  Make a new ability"
        print "3:  Make a new attribute"
        print "4:  Make a new biome"
        print "5:  Make a new element"
        print "6:  Make a new intelligence class"
        print "7:  Make a new origin type"
        print "8:  Make a new species class"
        print "9:  Display fields"
        print "10: Print all monsters"
        print "11: Clear database"
        print "Q:  Quit"
        user_input = raw_input("-->").lower()
                
        if mu.check_quit(user_input): break
        
        elif user_input=="0":
            monster.make_monster(mon_db)
            wait()
        elif user_input=="1":
            monster.make_monster_prompt(mon_db)
            wait()
        elif user_input=="2":
            ability.make_ability(mon_db)
            wait()
        elif user_input=="3":
            attribute.make_attribute(mon_db)
            wait()
        elif user_input=="4":
            biome.make_biome(mon_db)
            wait()
        elif user_input=="5":
            element.make_element(mon_db)
            wait()
        elif user_input=="6":
            intelligence.make_intelligence(mon_db)
            wait()
        elif user_input=="7":
            origin.make_origin(mon_db)
            wait()
        elif user_input=="8":
            species.make_species(mon_db)
            wait()
        elif user_input=="9":
            mon_db.mdprint()
            wait()
        elif user_input=="10":
            mon_db.print_monsters()
            wait()
        elif user_input=="11":
            while not mu.validate_yn(user_input):
                user_input = raw_input("Are you sure you want to clear the database?").lower()
            if user_input=='y' or user_input=="yes":
                mon_db.reset()
                mon_db.save()
                wait()
        else:
            "I'm sorry, I don't recognize that input."
                

def main():
    mon_db = mdb.mon_database()
    
    if os.path.isfile("mdb"):
        print "Loading database"
        with open('mdb', 'rb') as saved:
            mon_db=pickle.load(saved)
    else:
        print "Generating database"
        mon_db.save()
    
    main_menu(mon_db)
    mon_db.save()
    return
      
       
        
if __name__ == '__main__':
    main()