import mongen_utilities as mu
__metaclass__ = type

class element:
        
    def __init__(self, n=""):
        self.name = n
        self.desc = ""
    def fprint(self, indent):
        print indent+"Name: "+self.name
        print indent+"Description: "+self.desc
        print
        
def detect_quit(elm):
    if "quit" in elm:
        return True
    else:
        return False
    
def make_elements(mon_db, source_class, indent=""):
    element_str = raw_input(indent+"Enter the elements of this "+source_class+", separated by semicolons: ")
    if element_str == "": return []
    elif mu.check_quit(element_str): 
        quit_set = set()
        quit_set.add("quit")
        return quit_set
    element_str = element_str.replace(",", ";")
    element_str_array = element_str.split(";")
    element_set = set()
    for e in element_str_array:
        e = e.strip()
        new_element = make_element(mon_db, e, derived=True, indent=indent)
        if type(new_element)==type("quit"):
            if new_element=="quit":
                quit_set = set()
                quit_set.add("quit")
                return quit_set
            else:
                raise TypeError
        element_set.add(new_element)
    
    if None in element_set: element_set.remove(None)
    return element_set

def make_element(mon_db, name="", derived=False, indent=""):
    new_element = element(name)
    while mon_db.check_if_exists(new_element, warn=False)==True:
        if derived==False:
            new_element.name = raw_input(indent+"Enter the full name of your element, or type quit to cancel: ").lower()
            if mu.check_quit(new_element.name): return
            if not mon_db.check_if_exists(new_element):
                break
        else:
            if name!="":
                print indent+"Found element "+name+". Adding."
                new_element = mon_db.elements[name]
                return new_element
            else:
                return
    new_element.desc = raw_input(indent+"Enter a description of "+new_element.name+": ")
    if new_element.desc=="": new_element.desc = new_element.name
    mon_db.check_if_exists(new_element, addme=True) 
    return new_element