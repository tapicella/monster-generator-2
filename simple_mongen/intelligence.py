import mongen_utilities as mu
__metaclass__ = type


class intelligence:
        
    def __init__(self, n="", d=""):
        self.name = n
        self.desc = d
    def fprint(self, indent):
        print indent+"Name: "+self.name
        print indent+"Description: "+self.desc
        print
        
#===============================================================================
# def make_intelligences(mon_db, source_class):
#     intelligence_str = raw_input("Enter the intelligences of this "+source_class+", separated by semicolons: ")
#     if intelligence_str == "": return []
#     intelligence_str = intelligence_str.replace(",", ";")
#     intelligence_str_array = intelligence_str.split(";")
#     intelligence_array = []
#     for e in intelligence_str_array:
#         e = e.strip()
#         new_intelligence = make_intelligence(mon_db, e, derived=True)
#         intelligence_array.append(new_intelligence)
#     return intelligence_array
#===============================================================================

def make_intelligence(mon_db, name="", derived=False):
    new_intelligence = intelligence(name)
    while mon_db.check_if_exists(new_intelligence, warn=False)==True:
        if derived==False:
            new_intelligence.name = raw_input("Enter the full name of your intelligence, or type quit to cancel: ").lower()
            if mu.check_quit(new_intelligence.name): return
            if not mon_db.check_if_exists(new_intelligence):
                break
        else:
            if name!="":
                print "Found intelligence "+name+". Adding."
                new_intelligence = mon_db.intelligences[name]
                return new_intelligence
            else:
                return
    new_intelligence.desc = raw_input("Enter a description of "+new_intelligence.name+": ")
    mon_db.check_if_exists(new_intelligence, addme=True) 
    return new_intelligence