import ability
import attribute
import biome
import element
import intelligence
import origin
import species
import monster
import pprint
import os
import cPickle as pickle
__metaclass__ = type


class mon_database:
    def __init__(self):
        self.abilities={}
        self.attributes={}
        self.biomes={}
        self.elements={}
        self.intelligence={"mindless": intelligence.intelligence("mindless", "Powered only by its creator's will, this being is incapable of independent action"),
                           "feral": intelligence.intelligence("feral", "Wild and instinctive, powerful magic is needed to control this beast"),
                           "trainable": intelligence.intelligence("trainable", "Patience and skill will easily enable this monsters to obey simple commands"),
                           "communicative": intelligence.intelligence("communicative", "This monster is intelligent enough to convey its thoughts and understand some language"),
                           "semi-sentient": intelligence.intelligence("semi-sentient", "This creature is capable of learning basic language and tool making"),
                           "sentient": intelligence.intelligence("sentient", "This being has roughly human-level intelligence"),
                           "superhuman": intelligence.intelligence("superhuman", "Vastly more intelligent than your average human, this entity is not to be trifled with"),
                           }
        self.origin={"natural": origin.origin("natural", "A monster naturally found in the wilderness of Diamorti"),
                     "magical": origin.origin("magical", "A monster magically created by some mad wizard"),
                     "demonic": origin.origin("demonic", "A creature from the demon plane that has crossed over to our world"),
                     "nightmare": origin.origin("nightmare", "A being spawned from dreams and nightmares and given life by the power of unconscious will"),
                     "artificial": origin.origin("artificial", "An entity with no life of its own, only animated through the magic of another")
                     }
        self.species={}
    
        self.monsters={}
        self.monsters_by_biome={}
        self.monsters_by_species={}
        self.monsters_by_element={}
        self.monsters_by_intelligence={}
        self.monsters_by_origin={}
        self.all_fields = [("Abilities",     self.abilities), ("Attributes",     self.attributes), ("Biomes",     self.biomes), ("Elements",     self.elements), ("Intelligences",     self.intelligence), ("Origins",     self.origin), ("Species",     self.species), ("Monsters",     self.monsters)]
    
    def reset(self):
        self.__init__()
    
    def mdprint(self):
        
        for foo in self.all_fields:
            print foo[0]
            for bar in foo[1]:
                foo[1][bar].fprint("    ")
                
    
    def save(self):
        with open('mdb', 'wb') as output:
            print "Saving database"
            pickle.dump(self, output)
    
    
    def print_foo(self, foo):
        if foo is "ability": 
            base_db = self.abilities
        elif foo is "attribute": 
            base_db = self.attributes
        elif foo is "biome": 
            base_db = self.biomes
        elif foo is "element": 
            base_db = self.elements
        elif foo is "intelligence": 
            base_db = self.intelligence
        elif foo is "origin": 
            base_db = self.origin
        elif foo is "species": 
            base_db = self.species
        else:
            raise Exception("Invalid class input")
        pp = pprint.PrettyPrinter(intent=4)
        pp.pprint(base_db)
    
    def get_foo_database(self, foo):
        if type(foo) is ability.ability: 
            base_db = self.abilities
        elif type(foo) is attribute.attribute: 
            base_db = self.attributes
        elif type(foo) is biome.biome: 
            base_db = self.biomes
        elif type(foo) is element.element: 
            base_db = self.elements
        elif type(foo) is intelligence.intelligence: 
            base_db = self.intelligence
        elif type(foo) is origin.origin: 
            base_db = self.origin
        elif type(foo) is species.species: 
            base_db = self.species
        elif type(foo) is monster.monster: 
            base_db = self.monsters
        else:
            raise Exception("Invalid class input")
        return base_db
    
    def check_if_exists(self, foo, addme=False, warn=True):
        if foo.name =="":
            return True #Not allowed to enter an empty name
        base_db = self.get_foo_database(foo)
        if foo.name in base_db and addme==False:
            if warn:
                print foo.name+" already in database! Try a different name"
            return True
        elif foo.name in base_db and addme:
            return True
        elif foo.name not in base_db and addme:
            base_db[foo.name] = foo
            self.save()
            return False
        else:
            return False        
        
    def add_monster(self, monster):
        
        self.check_if_exists(monster, True)
        
        for ability in monster.abilities:
            if ability.name not in self.monsters_by_ability:
                self.monsters_by_ability[ability.name] = set()
                if monster not in self.monsters_by_ability[ability.name]:
                    self.monsters_by_ability[ability.name].add(monster)
        
        for attribute in monster.attributes:
            if attribute.name not in self.monsters_by_attribute:
                self.monsters_by_attribute[attribute.name] = set()
                if monster not in self.monsters_by_attribute[attribute.name]:
                    self.monsters_by_attribute[attribute.name].add(monster)
        
        for biome in monster.biomes:
            if biome.name not in self.monsters_by_biome:
                self.monsters_by_biome[biome.name] = set()
                if monster not in self.monsters_by_biome[biome.name]:
                    self.monsters_by_biome[biome.name].add(monster)
        
        for element in monster.elements:
            if element.name not in self.monsters_by_element:
                self.monsters_by_element[element.name] = set()
                if monster not in self.monsters_by_element[element.name]:
                    self.monsters_by_element[element.name].add(monster)
                    
        for species in monster.species:            
            if species.name not in self.monsters_by_species:
                self.monsters_by_species[species.name] = set()
                if monster not in self.monsters_by_species[species.name]:
                    self.monsters_by_species[species.name].add(monster)
        
        if monster.origin.name not in self.monsters_by_origin:
            self.monsters_by_origin[monster.origin.name] = set()
            if monster not in self.monsters_by_origin[monster.origin.name]:
                self.monsters_by_origin[monster.origin.name].add(monster)
            
        if monster.intelligence.name not in self.monsters_by_intelligence:
            self.monsters_by_intelligence[monster.intelligence.name] = set()
            if monster not in self.monsters_by_intelligence[monster.intelligence.name]:
                self.monsters_by_intelligence[monster.intelligence.name].add(monster)
        
        self.save()
    
    def print_monsters(self, monsters=["all"]):
        
        for monster in monsters:
            if monster=="all":
                for m in self.monsters:
                    self.monsters[m].fprint("    ")
                return
            else:
                if monster in self.monsters:
                    self.monsters[monster].fprint("    ")
                else:
                    print "Monster "+monster+" not found."
                    
    def print_monsters_simple(self, monsters=["all"]):
        
        for monster in monsters:
            if monster=="all":
                for m in self.monsters:
                    print(self.monsters[m].name)
                return
            else:
                if monster in self.monsters:
                    print(self.monsters[monster].name)
                else:
                    print "Monster "+monster+" not found."            
        
    def find_monsters(self, biomes=False, elements=False, intelligence=False, origin=False, species=False):
        results = set()
        
        if biomes:
            for b in biomes:
                for monster in self.monsters_by_biome[b]:
                    results.add(monster.name)
        if elements:
            temp = set()
            for e in elements:
                for monster in self.monsters_by_element[e]:
                    temp.add(monster.name)
            if len(results) is 0:
                results = temp.copy()
            else:
                results = results.intersection(temp)
                
        if species:
            temp = set()
            for s in species:
                for monster in self.monsters_by_species[s]:
                    temp.add(monster.name)
            if len(results) is 0:
                results = temp.copy()
            else:
                results = results.intersection(temp)
                
        if intelligence:
            temp = set()
            for i in intelligence:
                for monster in self.monsters_by_intelligence[i]:
                    temp.add(monster.name)
            if len(results) is 0:
                results = temp.copy()
            else:
                results = results.intersection(temp)
                
        if origin:
            temp = set()
            for o in origin:
                for monster in self.monsters_by_origin[o]:
                    temp.add(monster.name)
            if len(results) is 0:
                results = temp.copy()
            else:
                results = results.intersection(temp)
        
        return results